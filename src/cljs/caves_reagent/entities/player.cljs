(ns caves-reagent.entities.player
  (:require [caves-reagent.entities.core :refer [Entity]]
            [caves-reagent.entities.aspects.mobile :refer [Mobile move can-move?]]
            [caves-reagent.entities.aspects.digger :refer [Digger dig can-dig?]]
            [caves-reagent.entities.aspects.attacker :refer [Attacker attack]]
            [caves-reagent.entities.aspects.destructible :refer [Destructible take-damage]]
            [caves-reagent.world :refer [is-empty? is-walkable? get-tile-kind set-tile-floor set-tile-rubble get-entity-at]]
            [caves-reagent.coords :refer [destination-coords]]))

(defrecord Player [id img location size])

(defn- check-tile
  "Check that the tile at the destination passes the given predicate."
  [world dest pred]
  (pred (get-tile-kind world dest)))

(extend-type Player Entity
  (tick [this world]
        (if-let [action (get-in world [:entities :player :next-action])]
          (action (update-in world [:entities :player] dissoc :next-action))
          world)))

(extend-type Player Mobile
  (move [this world dest]
        {:pre [(can-move? this world dest)]}
        (assoc-in world [:entities :player :location] dest))
  (can-move? [this world dest]
             (is-walkable? world dest)))

(extend-type Player Digger
  (dig [this world dest]
       {:pre [(can-dig? this world dest)]}
       (set-tile-rubble world dest))
  (can-dig? [this world dest]
            (check-tile world dest #{:wall})))

(extend-type Player Attacker
  (attack [this world target]
          {:pre [(satisfies? Destructible target)]}
          (let [damage 1]
            (take-damage target world damage))))

(defn make-player [location]
  (->Player :player "img/dodo-128.png" location [1 1]))

(defn move-player [world dir]
  (let [player (get-in world [:entities :player])
        target (destination-coords (:location player) dir)
        entity-at-target (get-entity-at world target)]
    (cond
      entity-at-target (attack player world entity-at-target)
      (can-move? player world target) (move player world target)
      (can-dig? player world target) (dig player world target)
      :else world)))

(defn take-action [world action]
  (if-not (get-in world [:entities :player :next-action])
    (assoc-in world [:entities :player :next-action] action)
    world))
