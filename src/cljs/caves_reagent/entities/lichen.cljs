(ns caves-reagent.entities.lichen
  (:require [caves-reagent.entities.core :refer [Entity get-id]]
            [caves-reagent.entities.aspects.destructible :refer [Destructible take-damage]]
            [caves-reagent.world :refer [find-empty-neighbor]]))

(defrecord Lichen [id img location size hp])

(defn make-lichen [location]
  (->Lichen (get-id) "img/mushroom-128.png" location [1 1] 1))

(defn- should-grow []
  (< (rand) 0.0025))

(defn- grow [lichen world]
  (if-let [target (find-empty-neighbor world (:location lichen))]
    (let [new-lichen (make-lichen target)]
      (assoc-in world [:entities (:id new-lichen)] new-lichen))
    world))

(extend-type Lichen Entity
  (tick [this world]
        (if (should-grow)
          (grow this world)
          world)))

(extend-type Lichen Destructible
  (take-damage [{:keys [id] :as this} world damage]
               (let [damaged-this (update-in this [:hp] - damage)]
                 (if-not (pos? (:hp damaged-this))
                   (update-in world [:entities] dissoc id)
                   (assoc-in world [:entities id] damaged-this)))))
