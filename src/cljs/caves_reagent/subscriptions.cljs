(ns caves-reagent.subscriptions
  (:require [re-frame.core :refer [register-sub subscribe]])
  (:require-macros [reagent.ratom :refer [reaction]]))

(register-sub
  :initialized?
  (fn [db]
    (reaction (not (empty? @db)))))

(register-sub
  :current-screen
  (fn [db]
    (reaction (get-in @db [:ui :current-screen]))))

(register-sub
  :pause-panel
  (fn [db]
    (reaction (get-in @db [:ui :pause-panel]))))

(register-sub
  :config
  (fn [db]
    (reaction (:config @db))))

(register-sub
  :scale-up
  (fn [db]
    (let [config (reaction (:config @db))]
      (reaction (:scale-up @config)))))

(register-sub
  :game
  (fn [db]
    (reaction (:game @db))))

(register-sub
  :world
  (fn [db]
    (let [game (reaction (:game @db))]
      (reaction (:world @game)))))

(register-sub
  :player
  (fn [db]
    (let [world (reaction (get-in @db [:game :world]))
          entities (reaction (:entities @world))]
      (reaction (:player @entities)))))

