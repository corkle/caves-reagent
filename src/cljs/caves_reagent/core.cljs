(ns caves-reagent.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [caves-reagent.handlers]       ;; Require to define handlers
            [caves-reagent.subscriptions]  ;; Require to define subscriptions
;;             [caves-reagent.debug.debug]     ;; DEBUG FILES
            [caves-reagent.ui.input :refer [add-input-listeners]]
            [caves-reagent.ui.screens.start-screen :refer [start-screen]]
            [caves-reagent.ui.screens.win-screen :refer [win-screen]]
            [caves-reagent.ui.screens.lose-screen :refer [lose-screen]]
            [caves-reagent.ui.screens.play-screen :refer [play-screen]]
            [caves-reagent.world :refer [random-world]]))


;; Screen Panels
;; ===========================================================
(defn app-panel []
  (let [screen (rf/subscribe [:current-screen])
        config (rf/subscribe [:config])
        scale-up (:scale-up @config)
        toolbar-height (:toolbar-height @config)
        [w h] (:view-size @config)]
    (fn []
      [:div
       [:div#app {:style {:background "#FAE8DC"
                          :border "10px solid #313133"
                          :box-sizing "content-box"
                          :width (scale-up w)
                          :height (+ (scale-up h) 50)
                          :position "relative"
                          :display "flex"
                          :margin-top 30
                          :margin-left "auto"
                          :margin-right "auto"}}
        (case @screen
          :start [start-screen]
          :win [win-screen]
          :lose [lose-screen]
          :play [play-screen]
          [start-screen])]])))

(defn loading-panel []
  (let [ready? (rf/subscribe [:initialized?])]
    (fn []
      (if-not @ready?
        [:div "Initiazling ..."
         [:div
          [:button {:on-click #(rf/dispatch [:initialize-db])} "Load Data"]]]
        [app-panel]))))




(defn ^:export init []
  (rf/dispatch [:initialize-db])
  (r/render [loading-panel] (.getElementById js/document "app-panel"))
  (add-input-listeners))

