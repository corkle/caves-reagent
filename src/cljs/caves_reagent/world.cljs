(ns caves-reagent.world
  (:require [caves-reagent.coords :refer [neighbors]]))

;; Data Structures
;; ===========================================================

(defrecord World [tiles])
(defrecord Tile [kind img size])

(def tiles
  {:floor (->Tile :floor "img/grass-tile-128.jpg" [1 1])
   :rubble (->Tile :rubble "img/rubble-tile.png" [1 1])
   :wall  (->Tile :wall  "img/boulder-tile.png" [1 1])
   :bound (->Tile :bound "img/grass.jpg" [1 1])})

;; Helper Functions
;; ===========================================================

(defn- get-tile-from-tiles
  [tiles [x y]]
  (get-in tiles [y x]))

(defn- random-coord
  [world]
  (let [tiles (:tiles world)
        rows (count tiles)
        cols (count (first tiles))]
    [(rand-int cols) (rand-int rows)]))

;; World Generataion
;; ===========================================================

(defn- random-tiles
  [world-size]
  (let [[cols rows] world-size]
    (letfn [(random-tile []
                         (tiles (rand-nth [:floor :wall])))
            (random-row []
                        (vec (repeatedly cols random-tile)))]
      (vec (repeatedly rows random-row)))))

(defn- get-smoothed-tile
  [block]
  (let [tile-counts (frequencies (map :kind block))
        floor-threshold 5
        floor-count (get tile-counts :floor 0)
        result (if (>= floor-count floor-threshold)
                 :floor
                 :wall)]
    (tiles result)))

(defn- block-coords
  [x y]
  (for [dx [-1 0 1]
        dy [-1 0 1]]
    [(+ x dx) (+ y dy)]))

(defn- get-block
  [tiles x y]
  (map (partial get-tile-from-tiles tiles)
       (block-coords x y)))

(defn- get-smoothed-row
  [tiles y]
  (mapv #(get-smoothed-tile (get-block tiles % y))
        (range (count (first tiles)))))

(defn- get-smoothed-tiles
  [tiles]
  (mapv #(get-smoothed-row tiles %)
        (range (count tiles))))

(defn smooth-world
  [{:keys [tiles] :as world}]
  (assoc world :tiles (get-smoothed-tiles tiles)))

(defn random-world
  [world-size]
  (let [world (->World (random-tiles world-size))
        world (nth (iterate smooth-world world) 3)]
    world))

;; Querying World
;; ===========================================================

(defn- get-tile
  [world coord]
  (get-tile-from-tiles (:tiles world) coord))

(defn- get-tile-kind
  [world coord]
  (:kind (get-tile world coord)))

(defn- set-tile
  [world [x y] tile]
  (assoc-in world [:tiles y x] tile))

(defn set-tile-floor
  [world coord]
  (set-tile world coord (:floor tiles)))

(defn set-tile-rubble
  [world coord]
  (set-tile world coord (:rubble tiles)))

(defn get-entity-at
  [world coord]
  (first (filter #(= coord (:location %)) (vals (:entities world)))))

(defn is-empty-floor? [world coord]
  (and (#{:floor} (get-tile-kind world coord))
       (not (get-entity-at world coord))))

(defn is-walkable? [world coord]
  (let [tile-kind (get-tile-kind world coord)]
  (and (or (#{:floor} tile-kind) (#{:rubble} tile-kind))
       (not (get-entity-at world coord)))))

(defn is-empty? [world coord]
  (and (#{:floor} (get-tile-kind world coord))
       (not (get-entity-at world coord))))

(defn find-empty-tile
  [world]
  (loop [coord (random-coord world)]
    (if (is-empty? world coord)
      coord
      (recur (random-coord world)))))

(defn find-empty-neighbor
  [world coord]
  (let [candidates (filter #(is-empty? world %) (neighbors coord))]
    (when (seq candidates)
      (rand-nth candidates))))
