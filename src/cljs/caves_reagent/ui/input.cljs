(ns caves-reagent.ui.input
  (:require [re-frame.core :as rf]))

;; Process Keyboard Input
;; ====================================================================================
(defmulti process-input
  (fn [screen game keycode]
    screen))

(defmethod process-input :start [screen game keycode]
  (case keycode
    13 (rf/dispatch [:new-game])
    nil))

(defmethod process-input :win [screen game keycode]
  (case keycode
    13 (rf/dispatch [:go-to-screen :start])
    nil))

(defmethod process-input :lose [screen game keycode]
  (case keycode
    13 (rf/dispatch [:go-to-screen :start])
    nil))

(defmethod process-input :play [screen game keycode]
  (let [game-running? (:running? game)]
    (case keycode
;;       13 (rf/dispatch [:win-game])
      27 (rf/dispatch [:pause-game])
      80 (rf/dispatch [:pause-game])
      (if game-running?
        (case keycode
;;           84 (rf/dispatch [:smooth-world])
          87 (rf/dispatch [:move-player :n])
          65 (rf/dispatch [:move-player :w])
          83 (rf/dispatch [:move-player :s])
          68 (rf/dispatch [:move-player :e])
          nil)
        nil))))

(rf/register-handler
  :process-keypress-event
  (fn [db [_ keycode]]
    (let [current-screen (get-in db [:ui :current-screen])
          game (:game db)]
      (process-input current-screen game keycode)
      db)))

(defn- key-press-handler [event]
  (let [keycode (.-keyCode event)]
    (rf/dispatch [:process-keypress-event keycode])))

(defn add-input-listeners []
  (.addEventListener js/window "keydown" key-press-handler))
