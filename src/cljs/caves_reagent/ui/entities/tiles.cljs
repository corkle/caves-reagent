(ns caves-reagent.ui.entities.tiles
  (:require [re-frame.core :as rf]))

(defn- get-tile-maps [tiles]
  (reduce into [] (map (fn [row row-idx]
                         (map (fn [{:keys [img size]} col-idx]
                                {:id (str col-idx "," row-idx) :x col-idx :y row-idx :w (first size) :h (second size) :img img})
                              row
                              (iterate inc 0)))
                       tiles
                       (iterate inc 0))))

(defn get-viewport-tiles
  [tiles [start-x start-y end-x end-y]]
  (let [view-tile-vecs (map #(subvec % start-x end-x) (subvec tiles start-y end-y))]
    (get-tile-maps view-tile-vecs)))

(defn tile-entity
  [x y w h img]
  (let [scale-up (rf/subscribe [:scale-up])
        left (@scale-up x)
        top (@scale-up y)
        width (@scale-up w)
        height (@scale-up h)]
    (fn [x y w h img]
     [:image {:id (str x "," y)
              :src img
              :height height
              :width width
              :style {:position "absolute"
                      :top top
                      :left left}}])))
