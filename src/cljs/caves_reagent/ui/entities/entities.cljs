(ns caves-reagent.ui.entities.entities
  (:require [re-frame.core :as rf]))

(defn entity-el
  [start-x start-y entity]
  (let [scale-up (rf/subscribe [:scale-up])]
    (fn [start-x start-y entity]
      (let [[entity-x entity-y] (:location entity)
            [w h] (:size entity)
            x (- entity-x start-x)
            y (- entity-y start-y)
            left (@scale-up x)
            top (@scale-up y)
            width (@scale-up w)
            height (@scale-up h)]
        [:image {:id (str "entity-" (:id entity))
                 :src (:img entity)
                 :width width
                 :height height
                 :style {:position "absolute"
                         :top top
                         :left left
                         :z-index 800}}]))))
