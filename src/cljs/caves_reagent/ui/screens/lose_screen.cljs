(ns caves-reagent.ui.screens.lose-screen)

(def lose-screen-styles
  {:style {:width "100%"
           :margin 50
           :margin-top 100
           :display "flex"
           :flex-direction "column"
           :align-items "center"
           :justify-content "space-around"
           :text-align "center"
           :font-family "Lato"}})

(defn lose-screen
  []
  (fn []
    [:div#lose-screen lose-screen-styles
     [:div
     [:h2 "Oh dear! The world has been consumed by fungi."]]
     [:div
      [:img {:src "img/mushroom-128.png"}]
      [:h6 "Sorry, better luck next time."]
      [:h6 "Press " [:span.key-text "Enter"] " to try again."]]]))
