(ns caves-reagent.ui.screens.start-screen)

(def start-screen-styles
  {:style {:width "100%"
           :margin 50
           :display "flex"
           :flex-direction "column"
           :align-items "center"
           :justify-content "space-around"
           :font-family "Lato"
           :text-align "center"}})

(defn start-screen []
  (fn []
    [:div#start-screen start-screen-styles
     [:div {:style {:text-shadow "1px 1px 3px #333333"}}
      [:h4 "Welcome to"]
      [:h1 "ROGUE REAGENT"]]
     [:div
      [:img {:src "img/dodo-128.png"}]
      [:h6
       "Press " [:span.key-text "Enter"] " to play."]]]))
