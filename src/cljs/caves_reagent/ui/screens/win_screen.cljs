(ns caves-reagent.ui.screens.win-screen)

(def win-screen-styles
  {:style {:width "100%"
           :margin 50
           :margin-top 100
           :display "flex"
           :flex-direction "column"
           :align-items "center"
           :justify-content "space-around"
           :text-align "center"
           :font-family "Lato"}})

(defn win-screen
  []
  (fn []
    [:div#win-screen win-screen-styles
     [:div
      [:h2 "Congratulations, you win!"]]
     [:div
      [:img {:src "img/dodo-128.png"}]
      [:h6 "This game is a demo created to test out some features for another project. I may continue to add features over time. I hope you enjoyed playing!"]
      [:h6 "Press " [:span.key-text "Enter"] "to play again."]]]))
