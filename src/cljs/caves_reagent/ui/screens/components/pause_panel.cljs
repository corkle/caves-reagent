(ns caves-reagent.ui.screens.components.pause-panel
  (:require [re-frame.core :refer [dispatch subscribe]]
            [caves-reagent.ui.screens.components.how-to-play :refer [how-to-play]]))

(def pause-backdrop-styles
  {:position "absolute"
   :z-index 999
   :top 0
   :left 0
   :width "100%"
   :min-height "100%"
   :background-color "rgba(51,51,51,0.7)"})

(def pause-main-menu-styles
  {:position "absolute"
   :width "100%"
   :min-height "100%"
   :display "flex"
   :flex-direction "column"
   :align-items "center"
   :justify-content "space-around"})


(defn- pause-main-menu []
  [:div#pause-main-menu {:style pause-main-menu-styles}
   [:div {:style {:flex 2
                  :padding-top 40
                  :display "flex"

                  :flex-direction "column"
                  :justify-content "center"}}
    [:div#game-paused-box {:style {:background-color "#FAE8DC"
                                   :border "8px solid #B02C20"
                                   :box-shadow "inset 0 -1px 10px 0px"}}
     [:h3 {:style {:color "#333333"
                   :padding 40
                   :margin 0
                   :text-shadow "1px 1px 3px #333333"
                   }}
      "GAME PAUSED"]]]

   [:div#pause-panel-buttons {:style {:flex 1
                                      :width "50%"
                                      :background-color "#FAE8DC"
                                      :border "5px solid #B02C20"
                                      :display "flex"
                                      :flex-direction "column"
                                      :justify-content "center"}}
    [:button  {:on-click #(dispatch [:pause-game])}
     "RESUME"]
    [:button {:on-click #(dispatch [:go-to-pause-panel :how-to-play])}
     "HOW TO PLAY"]
    [:button {:on-click #(dispatch [:go-to-screen :start])}
     "RESTART"]]
   [:div {:style {:flex 2}}]])

(defn- pause-panel []
  (let [pause-panel (subscribe [:pause-panel])]
    (fn []
      [:div#pause-panel-backdrop {:style pause-backdrop-styles}
       (case @pause-panel
         :main [pause-main-menu]
         :how-to-play [how-to-play]
         [pause-main-menu])])))
