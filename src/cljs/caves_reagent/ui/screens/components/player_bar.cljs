(ns caves-reagent.ui.screens.components.player-bar
  (:require [re-frame.core :refer [subscribe]])
  (:require-macros [reagent.ratom :refer [reaction]]))

(defn player-bar [[vcols vrows] entity-count]
  (let [config (subscribe [:config])
        scale-up (reaction (:scale-up @config))
        bar-h (:toolbar-height @config)]
    (fn [[vcols vrows]  entity-count]
      [:div#player-bar {:style {:background-color "#FAE8DC"
                                :box-shadow "inset 0 -1px 12px 2px #3D200B"
                                :border-top "3px solid #313133"
                                :position "relative"
                                :top (@scale-up vrows)
                                :left 0
                                :max-height bar-h
                                :width "100%"
                                :padding-bottom 3
                                :display "flex"
                                :flex-direction "row"
                                :align-items "center"
                                :justify-content "center"
                                :font-weight "bold"
                                :font-size 22
                                :color "#DD7327"
                                :font-family "Lato"}}
       [:span
        "Mushrooms Remaining:  "]
       (let [count-color (if (> entity-count 150) "#B02C20" "#333333")]
          [:span {:style {:margin-left 10
                          :color count-color}}
          entity-count])
       ])))
