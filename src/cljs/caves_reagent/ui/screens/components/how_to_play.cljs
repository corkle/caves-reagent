(ns caves-reagent.ui.screens.components.how-to-play
  (:require [re-frame.core :refer [dispatch]]))

(defn- instructions []
  [:div
   [:span.highlight "OBJECTIVE:"]
   [:p
    "The " [:img {:src "img/mushroom-icon.png" :width 25 :height 25}] " are growing out of control!"
    " Play as " [:img {:src "img/dodo-icon.png" :width 25 :height 25}] " to destroy them all before they consume the world."]
   [:p
    "To win you must bring the " [:img {:src "img/mushroom-icon.png" :width 25 :height 25}] " count to 0."
    " You lose if the count ever reaches 250."]

   [:span.highlight "CONTROLS:"]
   [:ul
    [:li [:span.key "W"] "/" [:span.key "A"] "/" [:span.key "S"] "/" [:span.key "D"] " -  Move one square North / West / South / East"]
    [:li [:span.key "Esc"] "/" [:span.key "P"] " -  Pause Menu"]]
   [:p "Dig through a " [:img {:src "img/boulder-icon.png" :width 25 :heigh 25}] " by attempting to move into its square."]
   [:p "Attack a " [:img {:src "img/mushroom-icon.png" :width 25 :heigh 25}] " by attempting to move into its square."]])

(def how-to-play-styles
  {:position "absolute"
   :width "100%"
   :min-height "100%"
   :display "flex"
   :flex-direction "column"
   :align-items "center"
   :justify-content "space-around"})

(defn how-to-play []
  [:div#how-to-play {:style how-to-play-styles}

   [:div {:style {:background-color "#FAE8DC"
                  :border "5px solid #B02C20"
                  :width "70%"
                  :display "flex"
                  :flex-direction "column"
                  :text-align "center"
                  :align-items "center"
                  :justify-content "space-around"
                  :color "#333333"}}
    [:div {:style {:flex 1}}
[:h3 {:style {:text-shadow "1px 1px 3px #333333"}}
 "HOW TO PLAY"]]
    [:div#instructions {:style {:flex 3
                                :text-align "left"
                                :word-wrap "normal"
                                :margin-left 60
                                :margin-right 60}}
     [instructions]]
    [:div{:style {:flex 1}}
    [:button {:on-click #(dispatch [:go-to-pause-panel :main])} "Done"]]
    ]
   ])
