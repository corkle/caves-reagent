(ns caves-reagent.ui.screens.play-screen
  (:require [re-frame.core :as rf]
            [caves-reagent.ui.screens.components.pause-panel :refer [pause-panel]]
            [caves-reagent.ui.screens.components.player-bar :refer [player-bar]]
            [caves-reagent.ui.entities.tiles :refer [get-viewport-tiles tile-entity]]
            [caves-reagent.ui.entities.entities :refer [entity-el]])
  (:require-macros [reagent.ratom :refer [reaction]]))

(defn- get-viewport-coords
  [location tiles view-size]
  (let [[center-x center-y] location
        [vcols vrows] view-size
        rows-count (count tiles)
        cols-count (count (first tiles))
        start-x (max 0 (- center-x (int (/ vcols 2))))
        start-y (max 0 (- center-y (int (/ vrows 2))))
        end-x (min (+ start-x vcols) cols-count)
        end-y (min (+ start-y vrows) rows-count)
        start-x (max 0 (- end-x vcols))
        start-y (max 0 (- end-y vrows))]
    [start-x start-y end-x end-y]))

(defn- stat-panel
  ([location vcoords] (stat-panel location vcoords nil))
  ([[x y] [start-x start-y end-x end-y] test-data]
  (let [config (rf/subscribe [:config])
        scale-up (:scale-up @config)
        [vcols vrows] (:view-size @config)
        left 20
        top (scale-up vrows)]
    (fn [[x y] [start-x start-y end-x end-y] test-data]
      [:div#stat-panel {:style {:position "absolute"
                                :left left
                                :top top
                                :margin-top 12}}
       [:p (str "loc: [" x "-" y"] start: [" start-x "-" start-y "] end: [" end-x "-" end-y "] shroom-count: " test-data)]]))))

(defn play-screen []
  (let [config (rf/subscribe [:config])
        view-size (reaction (:view-size @config))
        game (rf/subscribe [:game])
        running? (reaction (:running? @game))
        world (reaction (:world @game))
        tiles (reaction (:tiles @world))
        entities (reaction (:entities @world))
        player (reaction (:player @entities))
        viewport-coords (reaction (get-viewport-coords (:location @player) @tiles @view-size))
        viewport-tiles (reaction (get-viewport-tiles @tiles @viewport-coords))]
    (fn []
      [:div#play-screen {:style {:display "flex" :flex-direciton "column" :width "100%"}}
       ;; ------- PAUSE PANEL
       (if-not @running? [pause-panel])
       ;; ------- TILES
       (for [{:keys [id x y w h img]} @viewport-tiles]
         ^{:key id} [tile-entity x y w h img])
       ;; ------- ENTITIES
       (doall (for [{:as entity :keys [id location]} (vals @entities)
                    :let [[start-x start-y end-x end-y] @viewport-coords
                          [x y] location]
                    :when (and (< (dec start-x) x end-x) (< (dec start-y) y end-y))]
                ^{:key id} [:div#entities
                            [entity-el start-x start-y entity]]))
       ;; ------- TOOLBARS
       [player-bar @view-size (dec (count @entities))]
;;        [stat-panel (:location @player) @viewport-coords (dec (count @entities))]
       ])))
