(ns caves-reagent.handlers
  (:require [re-frame.core :refer [register-handler dispatch]]
            [caves-reagent.world :refer [random-world smooth-world find-empty-tile]]
            [caves-reagent.coords :refer [destination-coords]]
            [caves-reagent.entities.core :refer [tick]]
            [caves-reagent.entities.player :refer [make-player take-action move-player]]
            [caves-reagent.entities.lichen :refer [make-lichen]]))

;; Initialize App-State
;; ===========================================================

(register-handler
  :initialize-db
  (fn [_ _]
    (let [scale-up #(* 32 %)
          view-size [25 19]
          world-size [80 50]
          toolbar-height 50]
      {:config {:scale-up scale-up
                :view-size view-size
                :world-size world-size
                :toolbar-height toolbar-height}
       :ui {:current-screen :start
            :pause-panel :main}
       :game {:world {}
              :running? false}})))

;; Navigation
;; ===========================================================

(defn- go-to-screen
  [db screen]
  (assoc-in db [:ui :current-screen] screen))

(register-handler
  :go-to-screen
  (fn [db [_ screen]]
    (go-to-screen db screen)))

(register-handler
  :go-to-pause-panel
  (fn[db [_ panel]]
    (assoc-in db [:ui :pause-panel] panel)))

;; World Generation
;; ===========================================================

(defn- add-lichen
  [world]
  (let [{:as lichen :keys [id]} (make-lichen (find-empty-tile world))]
    (assoc-in world [:entities id] lichen)))

(defn- populate-world
  [world]
  (let [player (make-player (find-empty-tile world))
        world (assoc-in world [:entities :player] player)
        world (nth (iterate add-lichen world) 30)]
    world))

(register-handler
  :smooth-world
  (fn [db _]
    (update-in db [:game :world] smooth-world)))

;; Game Logic
;; ===========================================================

(register-handler
  :lose-game
  (fn [db _]
    (-> db
        (assoc-in [:game :running?] false)
        (go-to-screen :lose))))

(register-handler
  :win-game
  (fn [db _]
    (-> db
        (assoc-in [:game :running?] false)
        (go-to-screen :win))))

(defn- check-win-lose
  [db]
  (let [entity-count (count (get-in db [:game :world :entities]))]
    (cond
      (<= entity-count 1) (do (dispatch [:win-game]) db)
      (>= entity-count 250) (do (dispatch [:lose-game]) db)
      :else db)))

(defn- tick-entity
  [world entity]
  (tick entity world))

(defn- tick-all
  [world]
  (reduce tick-entity world (vals (:entities world))))

(register-handler
  :tick-loop
  (fn [db _]
    (let [running? (get-in db [:game :running?])]
      (if running?
        (do
          (js/setTimeout #(dispatch [:tick-loop]) 75)
          (-> db
              (update-in [:game :world] tick-all)
              (check-win-lose)
              ))
        db))))

(register-handler
  :pause-game
  (fn [db _]
    (dispatch [:tick-loop])
    (update-in db [:game :running?] not)))

(register-handler
  :new-game
  (fn [db _]
    (let [world-size (get-in db [:config :world-size])
          new-world (random-world world-size)]
      (do
        (dispatch [:tick-loop])
        (-> db
            (assoc-in [:game :world] new-world)
            (update-in [:game :world] populate-world)
            (assoc-in [:game :running?] true)
            (go-to-screen :play))))))

;; Player Actions
;; ===========================================================

(register-handler
  :move-player
  (fn [db [_ dir]]
;;     (update-in db [:game :world] move-player dir)
    (update-in db [:game :world] take-action #(move-player % dir))
    ))
